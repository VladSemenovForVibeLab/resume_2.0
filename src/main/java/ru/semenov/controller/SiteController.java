package ru.semenov.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SiteController {
    @GetMapping("/javaDeveloperSemenovVladislav")
    public String getMain() {
        return "index";
    }
}
